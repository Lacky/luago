#include "actions.hpp"
#include "config.hpp"
#include "turtle.hpp"
#include <iostream>
#include <cmath>

// ======== Forward ========
Forward::Forward(int len) : 
    Action(), lastStep(len), progress(0), done(false){}

Forward::~Forward(){}

void Forward::step(){
    if (done) return;

    int currentStep = 0;

    if (lastStep < 0) {
        progress -= Config::stepLength;
        currentStep = -Config::stepLength;
    }
    else {
        progress += Config::stepLength;
        currentStep = Config::stepLength;
    }

    if (abs(progress) >= abs(lastStep)){
        currentStep = lastStep-(progress-currentStep);
        done = true;
    }

    Turtle::forward(currentStep);
}

bool Forward::finished(){
    return done;
}
// =========================

// ========= Rotate ========
Rotate::Rotate(double angle) : 
    Action(), lastStep(angle), progress(0), done(false){

    std::cout << "Rotate has been created. Angle " << angle << std::endl;
}

Rotate::~Rotate(){
    std::cout << "Rotate done. Current angle: " << Turtle::getAngle() << std::endl;
}

void Rotate::step(){
    if (done) return;
    double currentStep = static_cast<double>(Config::stepLength)/4;
    progress += currentStep;
    if (progress >= lastStep){
        currentStep = lastStep-(progress-currentStep);
        done = true;
    }
    Turtle::rotate(currentStep);
}

bool Rotate::finished(){
    return done;
}
// =========================

// ======== Circle =========

Circle::Circle(int r) :
    Action(), radius(r), progress(0), lastStep(2*3.14*r), done(false),
    perimeter(2*3.14*r), rotateTurn(false){

    std::cout << "Circle has been created. Radius " << radius << std::endl;
}

Circle::~Circle(){
    std::cout << "Circle done\n";
}

void Circle::step(){
    if (done) return;
    
    for (int i=1; (i<=Config::stepLength) and !done; i++){
        if (rotateTurn){
            double currentStep = 6.28/360.0;
            if (radius < 0) currentStep = -currentStep;
            Turtle::rotate(currentStep);
            rotateTurn = false;
        }
        else {
            double currentStep = perimeter/360.0;
            progress += currentStep;
            if (fabs(progress) >= fabs(lastStep)){
                currentStep = lastStep-(progress-currentStep);
                done = true;
            }
            int len = currentStep*2;
            if (len < 0) len = -len;
            Turtle::forward(len);
            rotateTurn = true;
        }
    }
}

bool Circle::finished(){
    return done;
}

// =========================

// === Enable/disable pen ==

EnablePen::EnablePen(bool enable) :
    Action(), enable(enable), done(false){

}

EnablePen::~EnablePen(){}

void EnablePen::step(){
    if (done) return;
    Turtle::enablePen(enable);
    if (enable) std::cout << "Pen has been enabled\n";
    else std::cout << "Pen has been disabled\n";
    done = true;
}

bool EnablePen::finished(){
    return done;
}

// =========================

// ======= Set color =======

SetColor::SetColor(uint8_t r, uint8_t g, uint8_t b):
    Action(), r(r), g(g), b(b), done(false){

}

SetColor::~SetColor(){}

void SetColor::step(){
    if (done) return;
    Turtle::setColor(r, g, b);
    std::cout << "New color: R=" << (int)r <<" G=" << (int)g << " B=" << int(b) << std::endl;
    done = true;
}

bool SetColor::finished(){
    return done;
}

// =========================