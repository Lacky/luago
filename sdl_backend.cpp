#include "sdl_backend.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <cstring>
#include <cassert>

static SDL_Window* window;
//SDL_Surface* screen;
static SDL_Renderer* renderer;

int SDL::init(int width, int height){
    if (SDL_Init(SDL_INIT_VIDEO) < 0) return 1;
    window = SDL_CreateWindow(
        "LUAgo",                  
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        width, height,
        SDL_WINDOW_OPENGL
    );
    if (window == nullptr) return 2;
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    if (renderer == nullptr) return 3;
    
    return 0;
}

SDL_Surface* SDL::getWindowSurface(){
    return SDL_GetWindowSurface(window);
}

void SDL::fillRect(SDL_Surface* surf, uint8_t r, uint8_t g, uint8_t b){
    SDL_FillRect(surf, nullptr, SDL_MapRGB(surf->format, r, g, b));
}

void SDL::renderFillRect(uint8_t r, uint8_t g, uint8_t b){
    uint8_t currentR, currentG, currentB, currentAlpha;
    SDL_GetRenderDrawColor(renderer, &currentR, &currentG, &currentB, &currentAlpha);
    SDL_SetRenderDrawColor(renderer, r, g, b, currentAlpha);
    SDL_RenderFillRect(renderer, nullptr);
    SDL_SetRenderDrawColor(renderer, currentR, currentG, currentB, currentAlpha);
}

void SDL::delay(uint32_t sleep){
    SDL_Delay(sleep);
}

SDL_Surface* SDL::loadImage(const std::string& path){
    return IMG_Load(path.c_str());
}

const char* SDL::getError(){
    const char* sdlError = SDL_GetError();
    if (strlen(sdlError) > 0) return sdlError;
    return IMG_GetError();
}

void SDL::quit(){
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void SDL::refreshRender(){
    SDL_RenderPresent(renderer);
}

SDL_Texture* SDL::textureFromSurface(SDL_Surface* surface){
    return SDL_CreateTextureFromSurface(renderer, surface);
}

int SDL::renderCopyEx(SDL_Texture* tex, SDL_Rect* rect, double angle){
    return SDL_RenderCopyEx(renderer, tex, nullptr, rect, angle, nullptr, SDL_FLIP_NONE);
}

int SDL::drawLine(int xStart, int yStart, int xEnd, int yEnd){
    return SDL_RenderDrawLine(renderer, xStart, yStart, xEnd, yEnd);
}

int SDL::setDrawColor(int r, int g, int b, int alpha){
    return SDL_SetRenderDrawColor(renderer, r, g, b, alpha);
}

SDL_Texture* SDL::createTexture(int w, int h){
    return SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, w ,h);
}

int SDL::switchRenderTarget(SDL_Texture* dest){
    return SDL_SetRenderTarget(renderer, dest);
}

SDL_Event* SDL::getEvent(){
    static SDL_Event e;
    if (SDL_PollEvent(&e)) return &e;
    else return nullptr;
}