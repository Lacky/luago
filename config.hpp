#ifndef _CONFIG_HPP_
#define _CONFIG_HPP_

namespace Config{
    extern int baseSpeed;
    extern int acceleration;
    extern int stepLength;
}

#endif