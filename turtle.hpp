#ifndef _TURTLE_HPP_
#define _TURTLE_HPP_

#include <queue>
#include <memory>

#include "sdl_backend.hpp"
#include "actions.hpp"

namespace Turtle{
    extern SDL_Surface* pix;
    extern SDL_Texture* tex;
    extern SDL_Rect params;
    extern double xPos, yPos;
    extern double angle;
    extern std::queue<std::shared_ptr<Action>> actions;
    extern bool penEnabled;

    extern void calcNewPosition(double angle, int len, double xStart, double yStart, double& xEnd, double& yEnd);
    extern double radToDeg(double rad);
    extern void setColor(int r, int g, int b);
    extern int draw();
    extern void getPosition(double& x, double& y);
    extern void setPosition(const double x, const double y);
    extern double getAngle();
    extern void rotate(double r);
    extern void forward(int len);
    extern void enablePen(bool on);
}

#endif