#ifndef _SDL_BACKEND_HPP_
#define _SDL_BACKEND_HPP_

#include <cstdint>
#include <string>
#include <SDL2/SDL.h>

namespace SDL {
    int init(int width, int height);
    void delay(uint32_t sleep);
    SDL_Surface* loadImage(const std::string& path);
    SDL_Surface* getWindowSurface();
    void fillRect(SDL_Surface* surf, uint8_t r, uint8_t g, uint8_t b);
    void renderFillRect(uint8_t r, uint8_t g, uint8_t b);
    const char* getError();
    void quit();
    void refreshRender();
    SDL_Texture* textureFromSurface(SDL_Surface* surface);
    int renderCopyEx(SDL_Texture* tex, SDL_Rect* rect, double angle);
    int drawLine(int xStart, int yStart, int xEnd, int yEnd);
    int setDrawColor(int r, int g, int b, int alpha=255);
    SDL_Texture* createTexture(int w, int h);
    int switchRenderTarget(SDL_Texture* dest);
    SDL_Event* getEvent();
}

#endif