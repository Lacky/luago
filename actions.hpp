#ifndef _ACTIONS_HPP_
#define _ACTIONS_HPP_

#include <cstdint>

class Action{
public:
    Action(){}
    virtual ~Action(){}
    virtual void step() = 0;
    virtual bool finished() = 0;
};

class Forward : public Action{
private:
    int lastStep;
    int progress;
    bool done;

public:
    Forward(int length);
    ~Forward();
    void step();
    bool finished();
};

class Rotate : public Action{
private:
    double lastStep;
    double progress;
    bool done;

public:
    Rotate(double angle);
    ~Rotate();
    void step();
    bool finished();
};

class Circle : public Action{
private:
    int radius;
    double progress;
    double lastStep;
    bool done;
    double perimeter;
    bool rotateTurn;

public:
    Circle(int radius);
    ~Circle();
    void step();
    bool finished();
};

class EnablePen : public Action{
private:
    bool enable;
    bool done;

public:
    EnablePen(bool enable);
    ~EnablePen();
    void step();
    bool finished();
};

class SetColor : public Action{
private:
    uint8_t r, g, b;
    bool done;

public:
    SetColor(uint8_t r, uint8_t g, uint8_t b);
    ~SetColor();
    void step();
    bool finished();
};

#endif