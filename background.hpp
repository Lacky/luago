#ifndef _BACKGROUND_HPP_
#define _BACKGROUND_HPP_

#include "sdl_backend.hpp"

namespace Background{
    extern SDL_Texture* bg;

    extern int init(int w, int h);
    extern int clear();
    extern int draw();
}

#endif