#include "lua_backend.hpp"
#include "turtle.hpp"

extern "C" {
    #include <lua5.3/lua.h>
    #include <lua5.3/lualib.h>
    #include <lua5.3/lauxlib.h>
}

static lua_State *state;
static std::string error = "";

int LUA::init(){
    state = luaL_newstate();
    if (!state) return 1;
    luaL_openlibs(state);
    lua_newtable(state);
    lua_setglobal(state, "turtle");
    LUA::initFunctions();

    return 0;
}

int LUA::loadFile(const std::string& path){
    return luaL_loadfile(state, path.c_str());
}

int LUA::run(){
    int ret = lua_pcall(state, 0, 0, 0);
    if (ret){
        error = lua_tostring(state, -1);
    }
    return ret;
}

std::string LUA::getError(){
    return error;
}

void LUA::registerFunction(lua_CFunction func, const std::string& name){
    lua_getglobal(state, "turtle");
    lua_pushstring(state, name.c_str());
    lua_pushcfunction(state, func);
    lua_settable(state, -3);
}

void LUA::quit(){
    lua_close(state);
}

void LUA::initFunctions(){
    LUA::registerFunction(LUA::forward, "forward");
    LUA::registerFunction(LUA::rotate, "rotate");
    LUA::registerFunction(LUA::circle, "circle");
    LUA::registerFunction(LUA::enablePen, "enablePen");
    LUA::registerFunction(LUA::setColor, "setColor");
}

//========== API for user ==========

int LUA::forward(lua_State* state){
    lua_Number length = lua_tonumber(state, 1);
    Turtle::actions.push(std::make_shared<Forward>(length));
    return 0;
}

int LUA::rotate(lua_State* state){
    lua_Number angle = lua_tonumber(state, 1);
    Turtle::actions.push(std::make_shared<Rotate>(angle));
    return 0;
}

int LUA::circle(lua_State* state){
    lua_Number radius = lua_tonumber(state, 1);
    Turtle::actions.push(std::make_shared<Circle>(radius));
    return 0;
}

int LUA::enablePen(lua_State* state){
    bool enable = lua_toboolean(state, 1);
    Turtle::actions.push(std::make_shared<EnablePen>(enable));
    return 0;
}

int LUA::setColor(lua_State* state){
    lua_Number r = lua_tonumber(state, 1);
    lua_Number g = lua_tonumber(state, 2);
    lua_Number b = lua_tonumber(state, 3);
    Turtle::actions.push(std::make_shared<SetColor>(r, g, b));
    return 0;
}

//==================================