#include "turtle.hpp"
#include <cmath>

SDL_Surface* Turtle::pix;
SDL_Texture* Turtle::tex;
SDL_Rect Turtle::params;
double Turtle::xPos, Turtle::yPos;
double Turtle::angle;
std::queue<std::shared_ptr<Action>> Turtle::actions;
bool Turtle::penEnabled;

void Turtle::calcNewPosition(double angle, int len, double xStart, double yStart, double& xEnd, double& yEnd){
    //I use law of sines to calculate new position
    //                *
    //               **
    //              * * 
    //             *  * 
    //            * 𝛾 *
    //           *    *
    //          *     *  
    //       a *      * c 
    //        *       * 
    //       *        *       
    //      *         *      
    //     *          *     
    //    *           *    
    //   *            *   
    //  * α          β*  
    // ****************
    //         b

    //angles in triangle
    double alpha = angle;
    const double beta = 3.14/2;
    double gamma = 3.14-alpha-beta;
    //the sides of the triangle
    double a = abs(len);
    // Division is unnecessary because sin (pi / 2) = 1
    // double b = a*sin(gamma)/sin(beta);
    // double c = a*sin(alpha)/sin(beta);
    double b = a*sin(gamma);
    double c = a*sin(alpha);
    if (len<0) {
        b = -b;
        c = -c;
    }
    xEnd = xStart+b;
    yEnd = yStart+c;
}

double Turtle::radToDeg(double rad){
    return rad*180/3.14;
}

void Turtle::setColor(int r, int g, int b){
    SDL::setDrawColor(r, g, b);
}

int Turtle::draw(){
    return SDL::renderCopyEx(Turtle::tex, &Turtle::params, Turtle::radToDeg(Turtle::angle));
}

void Turtle::getPosition(double& x, double& y){
    x = Turtle::xPos;
    y = Turtle::yPos;
}

void Turtle::setPosition(const double x, const double y){
    Turtle::xPos = x;
    Turtle::yPos = y;
    Turtle::params.x = x-(Turtle::params.w/2.0);
    Turtle::params.y = y-(Turtle::params.h/2.0);
}

void Turtle::rotate(double r){
    Turtle::angle += r;
}

void Turtle::forward(int len){
    double xStart, yStart;
    Turtle::getPosition(xStart, yStart);
    double xEnd, yEnd;
    Turtle::calcNewPosition(Turtle::angle, len, xStart, yStart, xEnd, yEnd);
    if (Turtle::penEnabled) SDL::drawLine(xStart, yStart, xEnd, yEnd);
    Turtle::setPosition(xEnd, yEnd);
}

double Turtle::getAngle(){
    return Turtle::angle;
}

void Turtle::enablePen(bool on){
    Turtle::penEnabled = on;
}