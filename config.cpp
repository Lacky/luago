#include "config.hpp"

int Config::baseSpeed = 2;
int Config::acceleration = 1;
int Config::stepLength = Config::baseSpeed*Config::acceleration;