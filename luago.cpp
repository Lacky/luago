#include <iostream>
#include <cstdlib>
#include <cstdint>

extern "C" {
    #include <lua5.3/lua.h>
}

#include "config.hpp"
#include "turtle.hpp"
#include "background.hpp"
#include "sdl_backend.hpp"
#include "lua_backend.hpp"
#include "actions.hpp"

#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768

void criticalExit(const std::string& msg, uint8_t code){
    std::cout << msg << std::endl;
    exit(code);
}

void calculateStepLength(){
    Config::stepLength = Config::baseSpeed*Config::acceleration;
}

void init(){
    int initRet = SDL::init(WINDOW_WIDTH, WINDOW_HEIGHT);
    if (initRet != 0){
        std::cout << "Cannot initialize SDL\n";
        criticalExit(SDL::getError(), initRet);
    }
    initRet = Background::init(WINDOW_WIDTH, WINDOW_HEIGHT);
    if (initRet) criticalExit(SDL::getError(), 6);
    Background::clear();
    
    Turtle::pix = SDL::loadImage("turtle.png");
    if (Turtle::pix == nullptr) criticalExit(SDL::getError(), 4);
    Turtle::tex = SDL::textureFromSurface(Turtle::pix);
    if (Turtle::tex == nullptr) criticalExit(SDL::getError(), 5);
    Turtle::angle = 0;
    Turtle::enablePen(true);
    Turtle::setPosition(WINDOW_WIDTH/2, WINDOW_HEIGHT/2);
    Turtle::params.w = 30;
    Turtle::params.h = 30;
    Turtle::setColor(0, 0, 0);

    initRet = LUA::init();
    if (initRet != 0) criticalExit("Cannot initialize LUA", initRet);

    calculateStepLength();
}

void execEvent(SDL_Event* e){
    switch(e->type){
        case SDL_QUIT:{
            exit(0);
            break;
        }
        case SDL_KEYDOWN:{
            SDL_KeyboardEvent* key = reinterpret_cast<SDL_KeyboardEvent*>(e);
            switch(key->keysym.scancode){
                case SDL_SCANCODE_RIGHT:
                    Config::acceleration++;
                    std::cout << "Acceleartion = " << Config::acceleration << std::endl;
                    calculateStepLength();
                    break;
                case SDL_SCANCODE_LEFT:
                    Config::acceleration--;
                    if (Config::acceleration < 1) Config::acceleration = 1;
                    std::cout << "Acceleartion = " << Config::acceleration << std::endl;
                    calculateStepLength();
                    break;
                default:
                    ;
            }
            break;
        }
        default:
            ;
    }
}

void mainLoop(){    
    while(1){
        SDL_Event* event = SDL::getEvent();
        if (event) execEvent(event);
        SDL::switchRenderTarget(Background::bg);
        if (Turtle::actions.size() > 0){
            std::shared_ptr<Action>& action = Turtle::actions.front();
            action->step();
            if (action->finished()) Turtle::actions.pop();
        }
        SDL::refreshRender();        
        SDL::switchRenderTarget(nullptr);
        Background::draw();
        if (Turtle::draw() != 0) criticalExit(SDL::getError(), 7);
        SDL::refreshRender();
        SDL::delay(33);
    }
}

int main(int argc, char* argv[]){
    if (argc < 2){
        std::cout << "Usage: ./luago script.lua\n";
        exit(1);
    }
    init();
    if ( LUA::loadFile(argv[1]) ){
        std::cout << "Cannot open file: " << argv[1] << std::endl;
        exit(2);
    }
    if (LUA::run()){
        criticalExit(LUA::getError(), 42);
    }
    mainLoop();
    LUA::quit();
    SDL::quit();
    return 0;
}