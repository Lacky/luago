# LUAgo

LOGO but with LUA included :)

![LUAgo demo](https://bitbucket.org/Lacky/luago/raw/aa991b9dd81c7b5464956a39667421159e2bcdef/test.gif)

This is only simple tech demo that shows how to control an application with LUA. Probably project will not be maintained in the future. Full API you can find in test.lua file.

Usage: ./luago script.lua

You can change speed of the turtle by pressing arrows (left and right) on your keyboard.

# Build

To compile you will need SDL2, SDL2_image, LUA 5.3. If you have all libraries, run "build.sh".

# License

Source code is distributed under MIT license. Image of the turtle I found [here](https://publicdomainvectors.org/en/free-clipart/Green-turtle-clip-art/57214.html) and 
is distributed under Public Domain license.