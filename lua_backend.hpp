#ifndef _LUA_BACKEND_HPP_
#define _LUA_BACKEND_HPP_

#include <string>

extern "C" {
    #include <lua5.3/lua.h>
}

namespace LUA{
    int init();
    int loadFile(const std::string& path);
    int run();
    std::string getError();
    void registerFunction(lua_CFunction func, const std::string& name);
    void quit();
    void initFunctions();

    //========== API for user ==========
    int forward(lua_State* state);
    int rotate(lua_State* state);
    int circle(lua_State* state);
    int enablePen(lua_State* state);
    int setColor(lua_State* state);
    //==================================
}

#endif 