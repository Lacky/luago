#include "background.hpp"

SDL_Texture* Background::bg = nullptr;

int Background::init(int w, int h){
    Background::bg = SDL::createTexture(w, h);
    if (Background::bg == nullptr) return 1;
    return 0;
}
int Background::clear(){
    if (Background::bg == nullptr) return 1;
    SDL::switchRenderTarget(Background::bg);
    SDL::renderFillRect(255, 255, 255);
    SDL::refreshRender();
    SDL::switchRenderTarget(nullptr);
    return 0;
}
int Background::draw(){
    int ret = SDL::renderCopyEx(Background::bg, nullptr, 0);
    return ret;
}
